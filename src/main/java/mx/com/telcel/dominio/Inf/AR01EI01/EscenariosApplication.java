package mx.com.telcel.dominio.Inf.AR01EI01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EscenariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(EscenariosApplication.class, args);
	}

}
